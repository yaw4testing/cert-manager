
#Load Libraries 
import OpenSSL
import ssl
from dateutil import parser
from datetime import date
from dotenv import dotenv_values
import json
import logging
import pymsteams


logging.basicConfig(filename='test.log',
                    filemode='a',
                    format='%(asctime)s %(name)s %(levelname)s %(message)s',
                    level=logging.INFO) 

#load environment variables 
config = dotenv_values(".env")
urls = json.loads(config['urls'])

#Create Functions
def get_days_expiry_for_url(url):
    try:
        cert=ssl.get_server_certificate((url, 443))
        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
        x509.get_notAfter()
        expiry_date = parser.parse(x509.get_notAfter().decode("utf-8"))
        days_to_expiry = expiry_date.date() - date.today()
        days_to_expiry = days_to_expiry.days
        return days_to_expiry
    except Exception as e:
        logging.info(e)

def alert_checker(days_to_expiry):
    
    myTeamsMessage = pymsteams.connectorcard("https://refinitiv.webhook.office.com/webhookb2/47ae17e1-852b-4b17-b283-52f6d8088f1a@71ad2f62-61e2-44fc-9e85-86c2827f6de9/IncomingWebhook/576f118c702d43a0963f273907518292/8d4eb00d-af98-4725-961c-9f4d7114da23")   
    try:
        #alert checker
        if days_to_expiry >= 30:
            logging.info("More days:{}".format(url))
            myTeamsMessage.title('Certificate on FQDNs Lifespan Information.')
            myTeamsMessage.text("{} days more for certificate to expire on:{}".format(days_to_expiry,url))
            myTeamsMessage.send()
        elif 10 <= days_to_expiry <= 14:
            logging.info('Please the certificate on {} has {} to expire. Act now.'.format(url,days_to_expiry))
            myTeamsMessage.title('Warning!, certificate near expiration.')
            myTeamsMessage.text('Please the certificate on {} has {} days to expire. Act now.'.format(url,days_to_expiry))
            myTeamsMessage.send()
        else:
            logging.info("Act now:{}".format(url))
            myTeamsMessage.title('This is critical. Please treat as urgent.')
            myTeamsMessage.text('Please the certificate lifespan on {} has {} days to expire. Please renew.'.format(url,days_to_expiry))
            myTeamsMessage.send()
    
    except Exception as e:
        logging.info(e)

for url in urls:
    days_to_expiry = get_days_expiry_for_url(url)
    alert_checker(days_to_expiry)